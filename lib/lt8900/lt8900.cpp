/*
 * lt8900.cpp
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 */

#include <Arduino.h>
#include <SPI.h>
#include <SerialDebug.h>

#include "lt8900.h"
#include "schnakebus.h"

Lt8900::Lt8900(SPIClass &spi, uint8_t csPin, uint8_t resetPin, uint8_t pkgPin) : m_spi(spi), m_csPin(csPin), m_resetPin(resetPin), m_pktPin(pkgPin)
{
	m_spi = spi;

	m_spiSettings._bitOrder = MSBFIRST;
	m_spiSettings._clock = 1000000;
	m_spiSettings._dataMode = SPI_MODE1;

	pinMode(m_pktPin, INPUT_PULLUP);
	pinMode(m_resetPin, OUTPUT);
	digitalWrite(m_resetPin, HIGH);
	pinMode(m_csPin, OUTPUT);
	digitalWrite(m_csPin, HIGH);
}

void Lt8900::reset()
{
	pinMode(m_resetPin, OUTPUT);
	digitalWrite(m_resetPin, LOW);
	delay(200);
	digitalWrite(m_resetPin, HIGH);
	delay(200);
}

void Lt8900::setPower(int pa, int gain)
{
	uint16_t value = (pa & 0b1111) << 12 | (gain & 0b1111) << 7;
	writeRegister(9, value, 7);
}

bool Lt8900::init(uint64_t syncword, uint8_t channel, uint8_t listenAddress, uint8_t power, uint8_t gain)
{
	m_syncword = syncword;
	m_channel = channel;
	m_listenAddress = listenAddress;
	m_power = power;
	m_gain = gain;

	initHardware();

	if(!checkHardware()) {
		return false;
	}

	writePower();
	writeChannel();

	return true;
}

uint16_t Lt8900::readRegister(uint8_t reg)
{
	m_spi.beginTransaction(m_spiSettings);
	digitalWrite(m_csPin, LOW);

	m_spi.transfer(REGISTER_READ | (REGISTER_MASK & reg));
	uint16_t value = m_spi.transfer16(0x0000);

	digitalWrite(m_csPin, HIGH);
	m_spi.endTransaction();

	return (value);
}

uint8_t Lt8900::writeRegister(uint8_t reg, uint16_t data, uint8_t wait)
{
	m_spi.beginTransaction(m_spiSettings);
	digitalWrite(m_csPin, LOW);

	uint8_t result = m_spi.transfer(REGISTER_WRITE | (REGISTER_MASK & reg));
	m_spi.transfer16(data);

	digitalWrite(m_csPin, HIGH);
	m_spi.endTransaction();

	if(wait) delayMicroseconds(wait);

	return result;
}

uint8_t Lt8900::writeRegister16(uint8_t addr, uint8_t v1, uint8_t v2, uint8_t wait)
{
	m_spi.beginTransaction(m_spiSettings);
	digitalWrite(m_csPin, LOW);

	uint8_t result = m_spi.transfer(addr);
	m_spi.transfer(v1);
	m_spi.transfer(v2);
	digitalWrite(m_csPin, HIGH);

	m_spi.endTransaction();

	if(wait) delayMicroseconds(wait);

	return result;
}

void Lt8900::initHardware()
{
	writeRegister(0, 0x6FE0, 7);  // Recommended value by PMmicro
	writeRegister(1, 0x5681, 7);  // Recommended value by PMmicro
	writeRegister(2, 0x6617, 7);  // Recommended value by PMmicro
	writeRegister(4, 0x9CC9, 7);  // Recommended value by PMmicro

	writeRegister(5, 0x6637, 7);  // Recommended value by PMmicro
	writeRegister(7, m_channel);
	writeRegister(8, 0x6C90, 7);  // Recommended value by PMmicro
	writeRegister(9, 0x1840, 7);  // PA Control register

	writeRegister(10, 0x7FFD, 7);  // Recommended value by PMmicro
	writeRegister(11, 0x0008, 7);  // Recommended value by PMmicro
	writeRegister(12, 0x0000, 7);  // Recommended value by PMmicro
	writeRegister(13, 0x48BD, 7);  // Recommended value by PMmicro
	writeRegister(22, 0x00FF, 7);  // Recommended value by PMmicro
	writeRegister(23, 0x8005, 7);  // Recommended value by PMmicro
	writeRegister(24, 0x0067, 7);  // Recommended value by PMmicro
	writeRegister(25, 0x1659, 7);  // Recommended value by PMmicro

	writeRegister(26, 0x19E0, 7);  // Recommended value by PMmicro
	writeRegister(27, 0x1300, 7);  // Recommended value by PMmicro
	writeRegister(28, 0x1800, 7);  // Recommended value by PMmicro

	writeRegister(32, 0b1001100010010000); //AAABBCCCDDEEFFFG  A preamble length, B, syncword length, c trailer length, d packet type
	writeRegister(33, 0x3FC7, 7);  // Recommended value by PMmicro
	writeRegister(34, 0x2000, 7);  // Recommended value by PMmicro
	writeRegister(35, 0x0300, 7);  //POWER mode,  bit 8/9 on = retransmit = 3x (default)

	writeRegister(36, 0x7236, 7);  // Sync R0
	writeRegister(37, 0x0000, 7);  // Recommended value by PMmicro
	writeRegister(38, 0x0000, 7);  // Recommended value by PMmicro
	writeRegister(39, 0x1809, 7);  // Sync R3
	writeRegister(40, 0x4401, 7);  // Recommended value by PMmicro
	//writeRegister(41, 0x8000 | 0x4000 | 0x2000 | 0x1000 | (1 << 11));
	writeRegister(41, 0xb000);
	writeRegister(42, 0xFDB0, 7);  // Recommended value by PMmicro
	writeRegister(43, 0x000F, 7);  // Recommended value by PMmicro
}

bool Lt8900::checkHardware()
{
	uint16_t value_0 = readRegister(0);
	uint16_t value_1 = readRegister(1);

	debugA("Registers: %04x-%04x", value_0, value_1);
	if((value_0 == 0x6fe0) && (value_1 == 0x5681)) {
		debugA("Radio module running correctly...");

		return(true);
	} else {
		debugA("Failed initializing the radio module...");
		return(false);
	}
}

void Lt8900::writeSyncword(uint8_t address)
{
	uint64_t sync = m_syncword;
	sync ^= ((uint64_t)address << 56 | (uint64_t)address << 48 | (uint64_t)address << 40 | (uint64_t)address << 32
	        | (uint64_t)address << 24 | (uint64_t)address << 16 | (uint64_t)address << 8 | (uint64_t)address);

	writeRegister(36, sync & 0xffff);
	writeRegister(37, (sync >> 16) & 0xffff);
	writeRegister(38, (sync >> 32) & 0xffff);
	writeRegister(39, (sync >> 48) & 0xffff);
}

void Lt8900::writeChannel()
{
	writeRegister(7, m_channel & 0x7f);
}

/*
void lt8900_init_hardware()
{
	writeRegister(0, 0x6fe0);
	writeRegister(1, 0x5681);
	writeRegister(2, 0x6617);
	writeRegister(4, 0x9cc9);    //why does this differ from powerup (5447)
	writeRegister(5, 0x6637);    //why does this differ from powerup (f000)
	writeRegister(8, 0x6c90);    //power (default 71af) UNDOCUMENTED

	writeRegister(9, 0x1840);
	writeRegister(10, 0x7ffd);   //bit 0: XTAL OSC enable
	writeRegister(11, 0x0008);   //bit 8: Power down RSSI (0=  RSSI operates normal)
	writeRegister(12, 0x0000);
	writeRegister(13, 0x48bd);   //(default 4855)

	writeRegister(22, 0x00ff);
	writeRegister(23, 0x8005);  //bit 2: Calibrate VCO before each Rx/Tx enable
	writeRegister(24, 0x0067);
	writeRegister(25, 0x1659);
	writeRegister(26, 0x19e0);
	writeRegister(27, 0x1300);  //bits 5:0, Crystal Frequency adjust
	writeRegister(28, 0x1800);

	//fedcba9876543210
	writeRegister(32, 0b1001100010010000); //AAABBCCCDDEEFFFG  A preamble length, B, syncword length, c trailer length, d packet type
	//                  E FEC_type, F BRCLK_SEL, G reserved
	//0x5000 = 0101 0000 0000 0000 = preamble 010 (3 bytes), B 10 (48 bits)
	writeRegister(33, 0x3fc7);
	writeRegister(34, 0x2000);  //
	writeRegister(35, 3 << 8);  //POWER mode,  bit 8/9 on = retransmit = 3x (default)

	//lt8900_setSyncWord(SYNC_WORD);

	writeRegister(40, 0x4401);  //max allowed error bits = 0 (01 = 0 error bits)
	writeRegister(41, 0x8000 | 0x4000 | 0x2000 | 0x1000 | (1 << 11));

	writeRegister(42, 0xfdb0);
	writeRegister(43, 0x000f);

	//lt8900_setCurrentControl(15, 0);

	writeRegister(50, 0x0000);  //TXRX_FIFO_REG (FIFO queue)

	writeRegister(52, 0x8080); //Fifo Rx/Tx queue reset

	delayMicroseconds(200);
	writeRegister(7, _BV(8));  //set TX mode.  (TX = bit 8, RX = bit 7, so RX would be 0x0080)
	delayMicroseconds(2);
	writeRegister(7, m_channel);  // Frequency = 2402 + channel
}
*/

void Lt8900::writePower()
{
	writeRegister(9, ((m_power & 0x0f) << 12) | ((m_gain & 0x0f) << 7));
}

bool Lt8900::hasData()
{
	return digitalRead(m_pktPin) > 0;
}

bool Lt8900::hasDataRegister()
{
	uint16_t value = readRegister(R_STATUS);
	if(bitRead(value,STATUS_CRC_BIT) != 0) {
		printlnV("LT8900: CRC error");
		listen();
		return false;
	}
	return((value & STATUS_PKT_BIT_MASK) > 0);
}

void Lt8900::listen()
{
	writeSyncword(m_listenAddress);
	writeChannel();
	delay(3);
	writeRegister(52, 0x0080); // Clear fifo bit
	writeRegister(7, (m_channel & 0x7f) | (1 << 7)); // Enable RX bit
}

int Lt8900::receive(uint8_t *buffer, size_t maxBufferSize)
{
	if(!hasData()) {
		return -2;
    }

	uint16_t status = readRegister(R_STATUS);
	if((status & 0x8000) == 0) { // Check CRC error
		uint16_t data = readRegister(R_FIFO);
		uint8_t length = data >> 8;
		if(length > maxBufferSize) {
			return -1;
		}

		uint8_t pos = 0;
		((uint8_t*)buffer)[pos++] = data & 0xff;
		while(pos < length) {
			data = readRegister(R_FIFO);
			((uint8_t*)buffer)[pos++] = data >> 8;
			((uint8_t*)buffer)[pos++] = data & 0xff;
		}

		return length;
	} else {
		// CRC error
		clearFifo();
		return -3;
	}
}

bool Lt8900::send(uint8_t address, const void *buffer, int length, bool wait)
{
	if(length < 1 || length > 255) {
		return false;
	}

	writeSyncword(address);

	writeRegister(7, 0);
	writeRegister(52, 0x8000);

	uint8_t pos = 0;
	uint8_t msb, lsb;
	writeRegister16(R_FIFO, (uint8_t)length, ((uint8_t*)buffer)[pos++], 7);
	while(pos < length) {
		msb = ((uint8_t*)buffer)[pos++];
		lsb = ((uint8_t*)buffer)[pos++];
		writeRegister16(50, msb, lsb, 7);
	}

	writeRegister(7, (m_channel & 0x7f) | (1 << 8));
	if(wait) {
		while(hasData()) {};
	}
	return true;
}

void Lt8900::clearFifo()
{
	writeRegister(52, 0x8080); //Fifo Rx/Tx queue reset
}
