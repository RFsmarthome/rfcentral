/*
 * lt8900.h
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 */

#ifndef LT8900_H_
#define LT8900_H_

#include <stdint.h>
#include <SPI.h>

#define REGISTER_READ       0b10000000  //bin
#define REGISTER_WRITE      0b00000000  //bin
#define REGISTER_MASK       0b01111111  //bin

#define CHANNEL_RX_BIT      7
#define CHANNEL_TX_BIT      8
#define CHANNEL_MASK        0b01111111  ///bin

#define STATUS_PKT_BIT_MASK	0x40

#define R_STATUS            48
#define STATUS_CRC_BIT      15

#define R_FIFO              50
#define R_FIFO_CONTROL      52



class Lt8900
{
public:
    Lt8900(SPIClass &spi, uint8_t csPin, uint8_t resetPin, uint8_t pkgPin);

    bool init(uint64_t syncword, uint8_t channel, uint8_t listenAddress, uint8_t power, uint8_t gain);
    void initHardware();
    bool checkHardware();

    void listen();
    bool hasData();
    bool hasDataRegister();

    int receive(uint8_t *buffer, size_t maxBufferSize);
    bool send(uint8_t address, const void *buffer, int length, bool wait);

    void clearFifo();
    void reset();

	void setPower(int pa, int gain);

private:
    uint16_t readRegister(uint8_t reg);
    uint8_t writeRegister(uint8_t reg, uint16_t data, uint8_t wait = 0);
    uint8_t writeRegister16(uint8_t addr, uint8_t v1, uint8_t v2, uint8_t wait = 0);
    void writeSyncword(uint8_t address);
    void writeChannel();
    void writePower();

    SPISettings m_spiSettings;

    uint64_t m_syncword;
    uint8_t m_channel;
    uint8_t m_listenAddress;

    uint8_t m_power;
    uint8_t m_gain;

    SPIClass &m_spi;

    uint8_t m_csPin;
    uint8_t m_resetPin;
    uint8_t m_pktPin;
};

#endif /* LT8900_H_ */
