#ifndef _NRF905_H
#define _NRF905_H

#include <Arduino.h>
#include <SPI.h>

#define NRF905_STATUS_DR 0b00100000
#define NRF905_STATUS_AM 0b10000000

#define NRF905_BUFFER_SIZE 32

class Nrf905
{
public:
	Nrf905(SPIClass &spi, int cs, int ce, int txe, int dr);

	bool init(const uint8_t address);

	void setReceiveAddress();
	void setReceiveAddress(const uint8_t address);
	void setDestinationAddress(const uint8_t address);

	void printAllRegisters();

	void setModeIdle();
	void setModeRx();
	void setModeTx();
	void enable();
	void disable();

	bool send(const uint8_t destination, const void *buffer, const uint8_t length);
	bool receive(void *buffer);

	bool isDataReady() const;
	bool waitDataReady(const int timeout=-1) const;

	uint8_t getStatus();

private:
	inline void beginSpi() {
		m_spi.beginTransaction(m_settings);
		digitalWrite(m_cspin, LOW);
	};
	inline void endSpi() {
		digitalWrite(m_cspin, HIGH);
		m_spi.endTransaction();
	};

	uint8_t readConfig(uint8_t reg, uint8_t &status);
	uint8_t writeConfig(uint8_t reg, uint8_t value);

	SPIClass &m_spi;

	int m_cspin;
	int m_cepin;
	int m_txepin;
	int m_drpin;

	SPISettings m_settings;

	uint8_t m_address;
};

#endif
