#include "nrf905.h"

#include <Arduino.h>
#include <SPI.h>
#include <SerialDebug.h>

#define CONFIG_MASK 0x0f
#define R_CONFIG 0x10
#define W_CONFIG 0x00 
#define W_TX_PAYLOAD 0b00100000
#define R_TX_PAYLOAD 0b00100001
#define W_TX_ADDRESS 0b00100010
#define R_TX_ADDRESS 0b00100011
#define R_RX_PAYLOAD 0b00100100
#define CHANNEL_CONFIG 0b10000000

Nrf905::Nrf905(SPIClass &spi, int cs, int ce, int txe, int dr) : m_spi(spi), m_cspin(cs), m_cepin(ce), m_txepin(txe), m_drpin(dr)
{
	m_settings._bitOrder = SPI_MSBFIRST;
	m_settings._clock = 1000000;
	m_settings._dataMode = SPI_MODE0;

	pinMode(m_cepin, OUTPUT);
	pinMode(m_txepin, OUTPUT);
	digitalWrite(m_cepin, LOW);
	digitalWrite(m_txepin, LOW);

	pinMode(m_cspin, OUTPUT);
	digitalWrite(m_cspin, HIGH);

	pinMode(m_drpin, INPUT_PULLUP);
}

void Nrf905::setReceiveAddress(const uint8_t address)
{
	m_address = address;
	setReceiveAddress();
}

void Nrf905::setReceiveAddress()
{
	writeConfig(5, 0xe7 ^ m_address);
	writeConfig(6, 0xe7 ^ m_address);
	writeConfig(7, 0xe7 ^ m_address);
	writeConfig(8, 0xe7 ^ m_address);
}

void Nrf905::setDestinationAddress(const uint8_t address)
{
	beginSpi();
	m_spi.transfer(W_TX_ADDRESS);
	m_spi.transfer(0xe7 ^ address);
	m_spi.transfer(0xe7 ^ address);
	m_spi.transfer(0xe7 ^ address);
	m_spi.transfer(0xe7 ^ address);
	endSpi();
}

bool Nrf905::init(const uint8_t address)
{
	m_address = address;
	writeConfig(0, 0x76);
	//writeConfig(1, 0b00001110); // 868
	writeConfig(1, 0b00001100); // 443
	writeConfig(2, 0b01000100);
	writeConfig(3, 0b00100000);
	writeConfig(4, 0b00100000);

	setReceiveAddress();

	writeConfig(9, 0b01011000);

	return false;
}

uint8_t Nrf905::readConfig(uint8_t reg, uint8_t &status)
{
	beginSpi();

	status = m_spi.transfer(R_CONFIG | (reg & CONFIG_MASK));
	uint8_t value = m_spi.transfer(0);

	endSpi();
	return value;
}

uint8_t Nrf905::writeConfig(uint8_t reg, uint8_t value)
{
	beginSpi();

	uint8_t status = m_spi.transfer(W_CONFIG | (reg & CONFIG_MASK));
	m_spi.transfer(value);

	endSpi();
	return status;
}

void Nrf905::printAllRegisters()
{
	uint8_t status;
	uint8_t value;
	debugA("Register [Nr]: [Value] [Status]");
	for(uint8_t r=0; r<10; ++r) {
		value = readConfig(r, status);
		debugA("Register %d: %02x %02x", r, value, status);
	}
}

uint8_t Nrf905::getStatus()
{
	beginSpi();
	uint8_t status = m_spi.transfer(0);
	endSpi();

	return status;
}

void Nrf905::setModeIdle()
{
	disable();
	setModeRx();
}

void Nrf905::enable()
{
	digitalWrite(m_cepin, HIGH);
}

void Nrf905::disable()
{
	digitalWrite(m_cepin, LOW);
}

void Nrf905::setModeRx()
{
	digitalWrite(m_txepin, LOW);
}

void Nrf905::setModeTx()
{
	digitalWrite(m_txepin, HIGH);
}

bool Nrf905::send(const uint8_t destination, const void *buffer, const uint8_t length)
{
	if(length <= NRF905_BUFFER_SIZE) {
		disable();
		delay(1);
		setModeTx();
		delay(1);
		const uint8_t *b = static_cast<const uint8_t*>(buffer);
		
		setDestinationAddress(destination);

		yield();

		beginSpi();
		m_spi.transfer(W_TX_PAYLOAD);
		for(uint8_t i=0; i<length; ++i) {
			m_spi.transfer(*b++);
		}
		endSpi();

		delay(1);
		enable();
		delay(5);

		return true;
	}
	return false;
}

bool Nrf905::receive(void *buffer)
{
	if(digitalRead(m_drpin)==HIGH) {
		bool rc = false;
		uint8_t *b = static_cast<uint8_t*>(buffer);

		beginSpi();
		uint8_t status = m_spi.transfer(R_RX_PAYLOAD);
		if(status && NRF905_STATUS_DR) {
			for(uint8_t i=0;i<32;++i) {
				*b++ = m_spi.transfer(0);
			}
			rc = true;
		}
		endSpi();
		return rc;
	}
	return false;
}

bool Nrf905::waitDataReady(const int timeout) const
{
	int count = 0;
	int maxCount = timeout / 100;
	while(!isDataReady()) {
		delay(100);
		yield();
		if((timeout > 0) && (++count > maxCount)) {
			return false;
		}
	}
	return true;
}

bool Nrf905::isDataReady() const
{
	return(digitalRead(m_drpin) == HIGH);
}
