#ifndef _SB2_H
#define _SB2_H

#include <stdint.h>

#define SB2_FLAGS_REQ 7
#define SB2_FLAGS_ACK 6
#define SB2_FLAGS_MASK_REQ (1 << SB2_FLAGS_REQ)
#define SB2_FLAGS_MASK_ACK (1 << SB2_FLAGS_ACK)
#define SB2_FLAGS_MASK_VERSION 0b00000011

#define SB2_MAX_PACKET_SIZE 32
#define SB2_MAX_PAYLOAD_SIZE 20

#define SB2_CRC_POLYNOM 0b10000111 // CRC-8 (ITU-T)

struct sb2_header_t
{
	uint8_t crc;
	uint8_t flags;
	uint8_t source;
	uint8_t id;
	uint8_t length;
};

enum sb2_type_t
{
	SB2_TEMPERATURE_T = 0,
	SB2_HUMIDITY_T = 1,
	SB2_PRESSURE_T = 2,
	SB2_RGB_T = 3,
	SB2_DIMMER_T = 4,
	SB2_BATTERY_T = 5,
	SB2_WRGB_T = 6
};

struct sb2_payload_t
{
	sb2_type_t type;
	uint8_t channel;
};

struct sb2_temperature_t
{
	int16_t temperature;
};

struct sb2_humidity_t
{
	int16_t humidity;
};

struct sb2_pressure_t
{
	int32_t pressure;
};

struct sb2_rgb_t
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

struct sb2_dimmer_t
{
	uint8_t value;
};

struct sb2_battery_t
{
	int16_t battery;
};

struct sb2_wrgb_t
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t w;
};

class SB2
{
public:
	SB2();

	bool fillBuffer(void *buffer, uint8_t length);
	uint8_t* getBuffer();

	bool checkPacket();
	bool isAckRequested();
	bool isAck();

	void setSourceAddress(uint8_t address);
	void setRequestAck();
	void setAck();
	void clearRequestAck();

	void setChannel(uint8_t channel);
	uint8_t generateId();

	void setTemperature(int16_t temp);
	void setHumidity(int16_t hum);
	void setPressure(int32_t pres);
	void setRGB(uint8_t r, uint8_t g, uint8_t b);
	void setDimmer(int8_t dimmer);
	void setBattery(int16_t bat);
	void setWRGB(uint8_t w, uint8_t r, uint8_t g, uint8_t b);

	uint8_t getType();
	uint8_t getChannel();
	uint8_t getLength();
	uint8_t getId();
	uint8_t getSource();
	uint8_t getCrc();
	uint8_t getFlags();

	bool checkCrc();
	void fillCrc();

private:
	uint8_t m_buffer[SB2_MAX_PACKET_SIZE];
	uint8_t m_length;
};

#endif
