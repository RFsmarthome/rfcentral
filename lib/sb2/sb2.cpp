#include <Arduino.h>
#include <FreeRTOS.h>
#include <SerialDebug.h>

#include "sb2.h"

SB2::SB2()
{
	memset(m_buffer, 0, SB2_MAX_PACKET_SIZE);
	m_length = 0;
}

bool SB2::checkCrc()
{
	uint8_t *msg = m_buffer;
	uint8_t length = m_length;

	uint8_t crc = 0xff;
	while(length--) {
		crc ^= *msg++;

		for(uint8_t i = 8; i; i--) {
			if(crc & 0x80) {
				crc = (crc << 1) ^ SB2_CRC_POLYNOM;
			} else {
				crc <<= 1;
			}
		}
	}

	return(crc == 0);
}

void SB2::fillCrc()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	header->crc = 0x00;

	uint8_t *msg = m_buffer;
	uint8_t length = m_length;

	uint8_t crc = 0xff;
	while(length--) {
		crc ^= *msg++;

		for(uint8_t i = 8; i; i--) {
			if(crc & 0x80) {
				crc = (crc << 1) ^ SB2_CRC_POLYNOM;
			} else {
				crc <<= 1;
			}
		}
	}

	header->crc = crc;
}

bool SB2::fillBuffer(void *buffer, uint8_t length)
{
	if(length > SB2_MAX_PACKET_SIZE) {
		return false;
	}

	m_length = length;
	memcpy(m_buffer, buffer, length);
	return true;
}

uint8_t* SB2::getBuffer()
{
	return m_buffer;
}

bool SB2::checkPacket()
{
	sb2_header_t *sb2 = reinterpret_cast<sb2_header_t*>(m_buffer);
	if(sb2->length > SB2_MAX_PAYLOAD_SIZE) {
		printlnD("Too large payload for valid packet");
		return false;
	}

	if(!checkCrc()) {
		printlnD("CRC8 not correct");
		return false;
	}

	return true;
}

bool SB2::isAckRequested()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	return(((header->flags & SB2_FLAGS_MASK_ACK) | (header->flags & SB2_FLAGS_MASK_REQ)) == (SB2_FLAGS_MASK_ACK | SB2_FLAGS_MASK_REQ));
}

bool SB2::isAck()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	return(((header->flags & SB2_FLAGS_MASK_ACK) | (header->flags & SB2_FLAGS_MASK_REQ)) == (SB2_FLAGS_MASK_ACK));
}

void SB2::setSourceAddress(uint8_t address)
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	header->source = address;
}

void SB2::setRequestAck()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	header->flags |= SB2_FLAGS_MASK_ACK | SB2_FLAGS_MASK_REQ;
}

void SB2::setAck()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	header->flags &= ~(SB2_FLAGS_MASK_REQ);
	header->flags |= SB2_FLAGS_MASK_ACK;
}

void SB2::clearRequestAck()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	header->flags &= ~(SB2_FLAGS_MASK_ACK | SB2_FLAGS_MASK_REQ);
}

void SB2::setChannel(uint8_t channel)
{
	sb2_payload_t *payload = reinterpret_cast<sb2_payload_t*>(m_buffer + sizeof(sb2_header_t));
	payload->channel = channel;
}

uint8_t SB2::generateId()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	header->id = rand() % 0xff;
	return(header->id);
}

void SB2::setTemperature(int16_t temp)
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	m_length = sizeof(sb2_header_t) + sizeof(sb2_payload_t) + sizeof(sb2_temperature_t);
	header->length = m_length;

	sb2_payload_t *payload = reinterpret_cast<sb2_payload_t*>(m_buffer + sizeof(sb2_header_t));
	payload->type = SB2_TEMPERATURE_T;

	sb2_temperature_t *value = reinterpret_cast<sb2_temperature_t*>(m_buffer + sizeof(sb2_header_t) + sizeof(sb2_payload_t));
	value->temperature = temp;

	fillCrc();
}

void SB2::setHumidity(int16_t hum)
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	m_length = sizeof(sb2_header_t) + sizeof(sb2_payload_t) + sizeof(sb2_humidity_t);
	header->length = m_length;

	sb2_payload_t *payload = reinterpret_cast<sb2_payload_t*>(m_buffer + sizeof(sb2_header_t));
	payload->type = SB2_HUMIDITY_T;

	sb2_humidity_t *value = reinterpret_cast<sb2_humidity_t*>(m_buffer + sizeof(sb2_header_t) + sizeof(sb2_payload_t));
	value->humidity = hum;

	fillCrc();
}

void SB2::setPressure(int32_t pres)
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	m_length = sizeof(sb2_header_t) + sizeof(sb2_payload_t) + sizeof(sb2_pressure_t);
	header->length = m_length;

	sb2_payload_t *payload = reinterpret_cast<sb2_payload_t*>(m_buffer + sizeof(sb2_header_t));
	payload->type = SB2_PRESSURE_T;

	sb2_pressure_t *value = reinterpret_cast<sb2_pressure_t*>(m_buffer + sizeof(sb2_header_t) + sizeof(sb2_payload_t));
	value->pressure = pres;

	fillCrc();
}

void SB2::setRGB(uint8_t r, uint8_t g, uint8_t b)
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	m_length = sizeof(sb2_header_t) + sizeof(sb2_payload_t) + sizeof(sb2_rgb_t);
	header->length = m_length;

	sb2_payload_t *payload = reinterpret_cast<sb2_payload_t*>(m_buffer + sizeof(sb2_header_t));
	payload->type = SB2_RGB_T;

	sb2_rgb_t *value = reinterpret_cast<sb2_rgb_t*>(m_buffer + sizeof(sb2_header_t) + sizeof(sb2_payload_t));
	value->r = r;
	value->g = g;
	value->b = b;

	fillCrc();
}

void SB2::setDimmer(int8_t dimmer)
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	m_length = sizeof(sb2_header_t) + sizeof(sb2_payload_t) + sizeof(sb2_dimmer_t);
	header->length = m_length;

	sb2_payload_t *payload = reinterpret_cast<sb2_payload_t*>(m_buffer + sizeof(sb2_header_t));
	payload->type = SB2_DIMMER_T;

	sb2_dimmer_t *value = reinterpret_cast<sb2_dimmer_t*>(m_buffer + sizeof(sb2_header_t) + sizeof(sb2_payload_t));
	value->value = dimmer;

	fillCrc();
}

void SB2::setBattery(int16_t bat)
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	m_length = sizeof(sb2_header_t) + sizeof(sb2_payload_t) + sizeof(sb2_battery_t);
	header->length = m_length;

	sb2_payload_t *payload = reinterpret_cast<sb2_payload_t*>(m_buffer + sizeof(sb2_header_t));
	payload->type = SB2_BATTERY_T;

	sb2_battery_t *value = reinterpret_cast<sb2_battery_t*>(m_buffer + sizeof(sb2_header_t) + sizeof(sb2_payload_t));
	value->battery = bat;

	fillCrc();
}

void SB2::setWRGB(uint8_t w, uint8_t r, uint8_t g, uint8_t b)
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	m_length = sizeof(sb2_header_t) + sizeof(sb2_payload_t) + sizeof(sb2_wrgb_t);
	header->length = m_length;

	sb2_payload_t *payload = reinterpret_cast<sb2_payload_t*>(m_buffer + sizeof(sb2_header_t));
	payload->type = SB2_WRGB_T;

	sb2_wrgb_t *value = reinterpret_cast<sb2_wrgb_t*>(m_buffer + sizeof(sb2_header_t) + sizeof(sb2_payload_t));
	value->r = r;
	value->g = g;
	value->b = b;
	value->w = w;

	fillCrc();
}

uint8_t SB2::getType()
{
	sb2_payload_t *payload = reinterpret_cast<sb2_payload_t*>(m_buffer + sizeof(sb2_header_t));
	return payload->type;
}

uint8_t SB2::getChannel()
{
	sb2_payload_t *payload = reinterpret_cast<sb2_payload_t*>(m_buffer + sizeof(sb2_header_t));
	return payload->channel;
}

uint8_t SB2::getLength()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	return header->length;
}

uint8_t SB2::getId()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	return header->id;
}

uint8_t SB2::getSource()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	return header->source;
}

uint8_t SB2::getCrc()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	return header->crc;
}

uint8_t SB2::getFlags()
{
	sb2_header_t *header = reinterpret_cast<sb2_header_t*>(m_buffer);
	return header->flags;
}
