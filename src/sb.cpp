#include "sb.h"

#include <SerialDebug.h>

Schnakebus::Schnakebus(AsyncMqttClient *mqtt, String &baseTopic, int qos, int retain)
{
	m_mqtt = mqtt;
	m_baseTopic = baseTopic;

	m_qos = qos;
	m_retain = retain;

	m_mutex = xSemaphoreCreateMutex();
}

void Schnakebus::setBuffer(void *buffer, int length)
{
	m_buffer = static_cast<uint8_t*>(buffer);
	m_length = length;
}

bool Schnakebus::isValidPacket()
{
	if(m_length == sizeof(buspacket_t)) {
		if(m_buffer[0]>=VERSION1 && m_buffer[0]<VERSION4) {
			m_buspacket = reinterpret_cast<buspacket_t*>(m_buffer);
			if(isCrcValid()) {
				printlnD("CRC in packet okay");
				return true;
			} else {
				printlnD("CRC corrupted");
			}
		} else {
			printlnI("No known version found");
		}
	} else {
		if(m_length == sizeof(buspacket4_t)) {
			printlnI("Possible buspacket4, not supported");
		} else {
			printlnD("False length of packet");
		}
	}
	return false;
}

bool Schnakebus::isCrcValid()
{
	uint8_t len = sizeof(buspacket_t);
	uint8_t *msg = m_buffer;

	uint8_t crc = 0x00;
	while(len--) {
		crc ^= *msg++;

		for(uint8_t i = 8; i; i--) {
			if(crc & 0x80) {
				crc = (crc << 1) ^ CRC_POLYNOM;
			} else {
				crc <<= 1;
			}
		}
	}

	debugD("CRC: %x", crc);
	return(crc==0);
}

uint16_t Schnakebus::send(String &topic, String &payload)
{
	uint16_t packetid = m_mqtt->publish(topic.c_str(), m_qos, m_retain, payload.c_str());
	debugD("Send with packet ID: %d", packetid);
	return  packetid;
}

bool Schnakebus::publish()
{
	String topic(m_baseTopic);
	topic += "/lt8900/";
	topic += String(m_buspacket->source);
	topic += "/";

	String t, payload;
	switch(m_buspacket->type) {
		case TYPE_BATTERY:
			//Serial.printf("(%d) Battery: %f\n", m_buspacket->source, m_buspacket->battery.battery / 1000.0);
			topic += "BATTERY/battery";
			payload = String(m_buspacket->battery.battery / 1000.0);
			send(topic, payload);
			break;

		case TYPE_BMP180:
			topic += "BMP180/";

			t = topic;
			t += "pressure";
			payload = String(m_buspacket->bmp180.pressure / 100.0);
			send(t, payload);

			t = topic;
			t += "temperature";
			payload = String(m_buspacket->bmp180.temperature / 10.0);
			send(t, payload);
			break;

		case TYPE_DHT22:
			topic += "DHT22/";

			t = topic;
			t += "humanity";
			payload = String(m_buspacket->dht22.humanity / 10.0);
			send(t, payload);

			t = topic;
			t += "temperature";
			payload = String(m_buspacket->dht22.temperature / 10.0);
			send(t, payload);
			break;

		case TYPE_DIMMER:
			printlnW("Type dimmer, not supported");
			break;

		case TYPE_HUMIDITY:
			topic += "HUMIDITY/humidity";
			payload = String(m_buspacket->humidity.humidity / 10.0);
			send(topic, payload);
			break;

		case TYPE_ON_OFF:
			topic += "ON_OFF/onoff";
			if(m_buspacket->onoff.onoff == 0) {
				payload = "OFF";
			} else {
				payload = "ON";
			}
			send(topic, payload);
			break;

		case TYPE_PRESSURE:
			topic += "PRESSURE/pressure";
			payload = String(m_buspacket->pressure.pressure / 100.0);
			send(topic, payload);
			break;

		case TYPE_RGB:
			printlnW("Type RGB, not supported");
			break;

		case TYPE_TEMPERATURE:
			topic += "TEMPERATURE/temperature";
			payload = String(m_buspacket->temperature.temperature / 10.0);
			send(topic, payload);
			break;

		case TYPE_WRGB1:
			printlnW("Type WRGB1, not supported");
			break;
			
		case TYPE_WRGB2:
			printlnW("Type WRGB1, not supported");
			break;
	}

	return true;
}
