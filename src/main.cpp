#include <Arduino.h>
#include <ArduinoJson.h>

#include <WiFi.h>
#include <WebServer.h>
#include <AutoConnect.h>
#include <ESPmDNS.h>
#include <SPI.h>
#include <SPIFFS.h>

#include <AsyncMqttClient.h>

#include <SerialDebug.h>

#include <freertos/task.h>
#include <freertos/portmacro.h>

//#include <DNSServer.h>
//#include <WebServer.h>
//#include <AutoConnect.h>

#include <schnakebus.h>
#include <lt8900.h>
#include <nrf905.h>

#include <freertos/semphr.h>

#include "sb.h"
#include "sb2.h"

extern "C" {
	#include <freertos/timers.h>
}

#define LT8900_CSPIN 27
#define LT8900_RESETPIN 26
#define LT8900_PKGPIN 25
#define SYNC_WORD 0x0123456789abcdef

#define NRF905_CEPIN 18
#define NRF905_TXEPIN 19
#define NRF905_CSPIN 16
#define NRF905_DRPIN 17

#define MY_ADDESS 0

const char thingName[] = "esp32";
const char thingApPasswort[] = "start";

const int ledPin = 2;

TimerHandle_t timerMqttReconnect;
String mqttPublishBase("rfcentral");
String mqttServer;
String mqttPort;
AsyncMqttClient mqttClient;

SPIClass g_spi = SPIClass(HSPI);
Lt8900 lt8900(g_spi, LT8900_CSPIN, LT8900_RESETPIN, LT8900_PKGPIN);
Nrf905 nrf905(g_spi, NRF905_CSPIN, NRF905_CEPIN, NRF905_TXEPIN, NRF905_DRPIN);

TaskHandle_t g_handleLt8900Task;
TaskHandle_t g_handleNrf905Task;

SemaphoreHandle_t g_spiMutex;
#define LOCKSPI(timeout) xSemaphoreTake(g_spiMutex, timeout);
#define UNLOCKSPI() xSemaphoreGive(g_spiMutex);

Schnakebus *schnakebus;

volatile uint8_t g_nrf905Int = 0;

AutoConnect portal;
AutoConnectConfig autoConnectConfig;

static const char AUX_mqtt_setting[] PROGMEM = R"raw(
	[
		{
			"title": "MQTT Settings",
			"uri": "/mqtt",
			"menu": "true",
			"element": [
				{
					"name": "caption",
					"type": "ACText",
					"value": "MQTT Settings page"
				},
				{
					"name": "mqttserver",
					"type": "ACInput",
					"value": "",
					"label": "Server",
					"placeholder": "MQTT broker server"
				},
				{
					"name": "mqttport",
					"type": "ACInput",
					"value": "",
					"label": "Port",
					"placeholder": "MQTT Port of the broker server"
				},
				{
					"name": "save",
					"type": "ACSubmit",
					"value": "Save",
					"uri": "/mqtt_save"
				}
			]
		},
		{
			"title": "MQTT Settings saved",
			"uri": "/mqtt_save",
			"menu": "false",
			"element": [
				{
					"name": "caption",
					"type": "ACText",
					"value": "Parameters saved"
				}
			]
		}
	]
)raw";


void isrHandleLt8900Pkg()
{
	BaseType_t taskWoken = pdFALSE;
	vTaskNotifyGiveFromISR(g_handleLt8900Task, &taskWoken);
	portYIELD_FROM_ISR();
}

void isrHandleNrf905Pkg()
{
	BaseType_t taskWoken = pdFALSE;
	vTaskNotifyGiveFromISR(g_handleNrf905Task, &taskWoken);
	portYIELD_FROM_ISR();
}



void onMqttConnect(bool sessionPresent)
{
	printlnI("MQTT server connected");
	xTimerStop(timerMqttReconnect, 0);
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason)
{
	if(WiFi.isConnected()) {
		printlnI("Start timer to connect to MQTT server");
		xTimerStart(timerMqttReconnect, 0);
	}
}

void onMqttPublish(uint16_t packetId)
{
	debugD("Publish acknowledged (%d)", packetId);
}

void connectMqtt()
{
	mqttClient.setServer(mqttServer.c_str(), mqttPort.toInt());

	printlnI("Connect to MQTT server");
	mqttClient.connect();
}

void setupMqtt()
{
	timerMqttReconnect = xTimerCreate("mqttTimer", pdMS_TO_TICKS(2000), pdTRUE, NULL, reinterpret_cast<TimerCallbackFunction_t>(connectMqtt));
	mqttClient.onDisconnect(onMqttDisconnect);
	mqttClient.onConnect(onMqttConnect);
	mqttClient.onPublish(onMqttPublish);
	xTimerStart(timerMqttReconnect, 0);
}

void handleRoot() {
  String  content =
    "<html>"
    "<head>"
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
    "</head>"
    "<body>"
    "<div class=\"menu\">" AUTOCONNECT_LINK(BAR_32) "</div>"
    "</body>"
    "</html>";

  WebServer &webServer = portal.host();
  webServer.send(200, "text/html", content);
}

void handleLT8900(void *pvParameters)
{
	for(;;) {
		// Block till wakeup
		ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

		if(lt8900.hasData()) {
			printlnV("Has data PIN set");
			uint8_t buffer[63];
			LOCKSPI(portMAX_DELAY);
			int length = lt8900.receive(buffer, 63);
			debugV("Length: %d", length);

			printlnV("Set LT8900 to listen");
			lt8900.clearFifo();
			lt8900.listen();
			UNLOCKSPI();

			schnakebus->setBuffer(buffer, length);
			if(schnakebus->isValidPacket()) {
				if(schnakebus->publish()) {
					printlnI("Data send to MQTT server");
				} else {
					printlnE("Error by sending data to MQTT server");
				}
			}
		}
	}
}

void handleNRF905(void *pvParameters)
{
	for(;;) {
		// Block till wakeup
		ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

		if(nrf905.isDataReady()) {
			LOCKSPI(portMAX_DELAY);
			nrf905.setModeIdle();
			printlnV("NRF905 has DataReady");
			uint8_t buffer[NRF905_BUFFER_SIZE];
			if(nrf905.receive(buffer)) {
				printlnV("NRF905 data received");
				Serial.println((char*)buffer);
			}
			nrf905.setModeRx();
			nrf905.enable();
			UNLOCKSPI();

			SB2 *sb2 = new SB2();
			sb2->fillBuffer(buffer, NRF905_BUFFER_SIZE);
			if(sb2->checkPacket()) {
				uint8_t source = sb2->getSource();
				if(sb2->isAckRequested()) {
					sb2->setAck();
					sb2->setSourceAddress(MY_ADDESS);
					sb2->fillCrc();

					LOCKSPI(portMAX_DELAY);
					nrf905.disable();
					nrf905.setModeTx();
					if(nrf905.send(source, sb2->getBuffer(), sb2->getLength())) {
						debugD("NRF905 send ACK for %d", source);
					} else {
						debugE("NRF905 error by sending AC for %d", source);
					}
					nrf905.waitDataReady();
					delay(1);
					nrf905.disable();
					nrf905.setModeRx();
					nrf905.enable();
					UNLOCKSPI();
				}
			}
		}
	}
}

void getParameters(AutoConnectAux &aux) {
	mqttServer = aux["mqttserver"].value;
	mqttServer.trim();

	mqttPort = aux["mqttport"].value;
	mqttPort.trim();
}

String loadParameters(AutoConnectAux &aux, PageArgument &args)
{
	File param = SPIFFS.open("/param.json", "r");
	if(param) {
		if(aux.loadElement(param)) {
			getParameters(aux);
			printlnA("Parameters loaded");
		} else {
			printlnA("Error by loading parameters");
		}
		param.close();
	} else {
		printlnA("Error by opening parameter file");
	}

	return String("");
}

String saveParameters(AutoConnectAux &aux, PageArgument &args)
{
	AutoConnectAux &mqttSettings = *portal.aux(portal.where());
	getParameters(mqttSettings);

	//AutoConnectInput &mqttserver = mqttSettings["mqttserver"].as<AutoConnectInput>();

	File param = SPIFFS.open("/param.json", "w");
	mqttSettings.saveElement(param, {"mqttserver", "mqttport"});
	param.close();

	return String("");
}

void setup() {
	delay(1000);
	Serial.begin(115200);

	SPIFFS.begin(true);

	pinMode(ledPin, OUTPUT);
	digitalWrite(ledPin, HIGH);

	printlnA("Start rfcentral...");
	WiFi.enableIpV6();
	String hostname = "esp-"+ String((long unsigned int)ESP.getEfuseMac(), HEX);
	autoConnectConfig.hostName = hostname;
	autoConnectConfig.apid = hostname;
	autoConnectConfig.autoReconnect = true;
	autoConnectConfig.portalTimeout = 300;
	autoConnectConfig.ticker = true;
	autoConnectConfig.tickerOn = LOW;
	autoConnectConfig.tickerPort = ledPin;
	if(portal.load(FPSTR(AUX_mqtt_setting))) {
		printlnA("Load AUX forms");
		AutoConnectAux &mqttSettings = *portal.aux("/mqtt");
		PageArgument args;
		loadParameters(mqttSettings, args);

		portal.on("/mqtt", loadParameters);
		portal.on("/mqtt_save", saveParameters);
	}
	portal.config(autoConnectConfig);
	if(portal.begin()) {
		debugA("Wifi connected: %s", WiFi.localIP().toString().c_str());
		WiFi.enableIpV6();
		MDNS.begin(hostname.c_str());
		MDNS.addService("http", "tcp", 80);

		setupMqtt();
	} else {
		printlnA("Timeout for captive portal. Restart ESP");
		ESP.restart();
	}
	WebServer &webServer = portal.host();
	webServer.on("/", handleRoot);

	g_spiMutex = xSemaphoreCreateMutex();

	mqttPublishBase += "/" + String((long unsigned int)ESP.getEfuseMac(), HEX);
	schnakebus = new Schnakebus(&mqttClient, mqttPublishBase);


	LOCKSPI(portMAX_DELAY);
	g_spi.begin();

	nrf905.init(MY_ADDESS);	// I'm the master
	nrf905.printAllRegisters();
	nrf905.setModeRx();
	nrf905.enable();

	lt8900.reset();
	delay(100);
	lt8900.init(0x0123456789abcdef, 120, MY_ADDESS, 7, 7);
	lt8900.listen();
	UNLOCKSPI();

	BaseType_t rc;
	rc = xTaskCreate(handleLT8900, "handleLt8900", 4096, NULL, 30, &g_handleLt8900Task);
	rc = xTaskCreate(handleNRF905, "handleNrf905", 4096, NULL, 30, &g_handleNrf905Task);

	attachInterrupt(LT8900_PKGPIN, isrHandleLt8900Pkg, RISING);
	attachInterrupt(NRF905_DRPIN, isrHandleNrf905Pkg, RISING);
}

void loop() {
	debugHandle();
	portal.handleClient();
}
