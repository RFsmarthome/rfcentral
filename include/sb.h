#ifndef _SB_H
#define _SB_H

#include <Arduino.h>
#include <AsyncMqttClient.h>

extern "C" {
	#include <freertos/semphr.h>
}

#include "schnakebus.h"

#define CRC_POLYNOM 0x5d

class Schnakebus
{
public:
	Schnakebus(AsyncMqttClient *mqtt, String &baseTopic, int qos=1, int retain=1);

	void setBuffer(void *buffer, int length);
	bool isValidPacket();

	bool publish();

private:
	bool isCrcValid();
	uint16_t send(String &topic, String &payload);

	String m_baseTopic;

	AsyncMqttClient *m_mqtt;
	uint8_t *m_buffer;
	buspacket_t *m_buspacket;
	int m_length;
	int m_qos;
	int m_retain;

	SemaphoreHandle_t m_mutex;
};

#endif
