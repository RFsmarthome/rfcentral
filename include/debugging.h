#ifndef _DEBUGGING_H
#define _DEBUGGING_H

#if DEBUGGING
	#define SP(s) Serial.print(F(s));
	#define SL(s) Serial.println(F(s));
	#define SF(s,v) Serial.printf(s,v);
#else
	#define SP(s)
	#define SL(s)
	#define SF(s,v)
#endif

#endif
